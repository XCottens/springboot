package com.ecommerce.microcommerce.controller;

import com.ecommerce.microcommerce.exceptions.ProduitIntrouvableException;
import com.ecommerce.microcommerce.dao.ProductDao;
import com.ecommerce.microcommerce.model.Product;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Api("API pour les opérations CRUD sur les produits.")
@RestController
public class ProductController {

	@Autowired
	private ProductDao productDao;

	@GetMapping("/Produits")
	public MappingJacksonValue listeProduits() {
		List<Product> produits = productDao.findAll();

		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("prixAchat", "id");

		FilterProvider filterList = new SimpleFilterProvider().addFilter("productFilter", filter);

		MappingJacksonValue produitsFiltres = new MappingJacksonValue(produits);

		produitsFiltres.setFilters(filterList);

		return produitsFiltres;
	}

	@ApiOperation("Récupère un produit grâce à son ID à condition que celui-ci soit en stock !")
	@GetMapping("/Produits/{id}")
	public Product afficherUnProduit(@PathVariable int id) {
		Product product = productDao.findById(id);

		if (product == null)
			throw new ProduitIntrouvableException(id);

		return product;
	}

	@PostMapping("/Produits")
	public ResponseEntity<Void> ajouterProduit(@Valid @RequestBody Product product){
		Product product1 = productDao.save(product);
		if (product1 == null) {
			return ResponseEntity.noContent().build();
		}

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(product1.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	@DeleteMapping ("/Produits/{id}")
	public void supprimerProduit(@PathVariable int id) {
		productDao.deleteById(id);
	}

	@PutMapping ("/Produits")
	public void updateProduit(@RequestBody Product product) {
		productDao.save(product);
	}

	@GetMapping("test/Produits/{prixLimite}")
	public List<Product> testRequete(@PathVariable int prixLimite){
		return productDao.chercherUnProduitCher(prixLimite);
	}
}
